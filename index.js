const express = require("express");
const mongoose = require("mongoose");
const cors = require("cors");

// !Add require routes here
// Require the routes
const entryRoutes = require("./routes/entryRoutes.js");
const productRoutes = require("./routes/productRoutes.js");
const userRoutes = require("./routes/userRoutes.js");

const app = express();


// Creates MongoDB connection
mongoose.connect("mongodb+srv://admin:admin123@zuitt.inau6u6.mongodb.net/Capstone3-API?retryWrites=true&w=majority", {
	useNewUrlParser: true,
	useUnifiedTopology: true
});

// Prompts message in the terminal once connection has error or open
const db = mongoose.connection;
db.on("error", console.error.bind(console, "Failed to connect to the database!"));
db.once("open", () => console.log("Now connected to MongoDB Atlas!"));

// Middlewares
app.use(cors());
app.use(express.json());
app.use(express.urlencoded({extended: true}));

// !Add initialization of routes here
// Initialization of the routes
app.use("/", entryRoutes);
app.use("/product", productRoutes);
app.use("/user", userRoutes);

const port = process.env.PORT || 4001;
app.listen(port, () => {
	console.log(`Capstone3-API is now online on port ${port}`)
})