const express = require("express");
const router = express.Router();
const User = require("../models/userModel.js");
const userControllers = require("../controllers/userControllers.js");
const auth = require("../auth.js");

// Get all user (admin only)
router.get("/all", auth.verify, (req, res) => {
	const data = {
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	}


	userControllers.getAllUsers(data).then(resultFromController => {res.send(resultFromController)
	})
})

// Get profile
router.post("/details", auth.verify, (req, res) => {
	// We can get the token by accessing req.headers.authorization
	const userData = auth.decode(req.headers.authorization)

	userControllers.getProfileNew({userId: userData.id}).then(resultFromController => res.send(resultFromController));
})


// Get single user - all information (admin only)
router.get("/:userId/profile", auth.verify, (req, res) => {
	const data = {
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	}

	userControllers.getUserProfile(data, req.params.userId).then(resultFromController => {
		res.send(resultFromController)
	})
})

// Get single user - filtered information
router.get("/:userId", (req, res) => {
	userControllers.getProfile(req.params.userId).then(resultFromController => {
		res.send(resultFromController)
	})
})

// Set a user as admin (admin only)
router.patch("/:userId/setAsAdmin", auth.verify, (req, res) => {
	const userData = {
		isSeller: auth.decode(req.headers.authorization).isSeller,
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	}

	userControllers.setAsAdmin(userData, req.params.userId).then(resultFromController => {
		res.send(resultFromController)
	})
})

// Set an admin as user (admin only)
router.patch("/:userId/setAsUser", auth.verify, (req, res) => {
	const userData = {
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	}

	userControllers.setAsUser(userData, req.params.userId).then(resultFromController => {
		res.send(resultFromController)
	})
})

// Deleting a User
router.delete("/:userId/delete", auth.verify, (req, res) => {
	const token = auth.decode(req.headers.authorization);

	userControllers.deleteUser(token, req.params.userId).then(resultFromController => {
		res.send(resultFromController)
	})
})

// Creating an order
router.post("/createOrder", auth.verify, userControllers.createOrder);


// Add to cart
router.post("/addToCart", auth.verify, (req, res) => {
	let data = {
		userId: auth.decode(req.headers.authorization).id,
		isAdmin: auth.decode(req.headers.authorization).isAdmin,
		productId: req.body.productId,
		quantity: req.body.quantity
	}
	console.log(data);

	userControllers.addToCart(data)
	.then(resultFromController => {
	res.send(resultFromController)
	})
})

// UserCart
router.get("/:userId/cart", (req, res) => {
	userControllers.getUserCart(req.params.userId).then(resultFromController => {
		res.send(resultFromController)
	})
})


/*
	TRIAL -----------------------------------------------------------------------
*/
// Add to cart
router.post("/trial/addToCart", auth.verify, (req, res) => {
	let data = {
		userId: auth.decode(req.headers.authorization).id,
		isAdmin: auth.decode(req.headers.authorization).isAdmin,
		productId: req.body.productId,
		quantity: req.body.quantity
	}

	userControllers.addToCartTrial(data)
	.then(resultFromController => {
	res.send(resultFromController)
	})
})

// UserCart
router.get("/trial/:userId/cart", (req, res) => {
	userControllers.getUserCartTrial(req.params.userId).then(resultFromController => {
		res.send(resultFromController)
	})
})

module.exports = router;