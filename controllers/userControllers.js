const User = require("../models/userModel.js");
const Product = require("../models/productModel.js");
const auth = require("../auth.js");
const bcrypt = require("bcrypt");

// Get All Users 
/*
@desc This is for admin role only. Admin role can view all users registered on the database.
*/
module.exports.getAllUsers = (data) => {
	if(data.isAdmin){
		return User.find({}, {
			password: 0
		}).then(result => {
		return result
		})
	}else {

		let message = Promise.resolve(false)
		return message.then((value) => {
			return value
		})
	}
}

// Get profile 
/*
@desc This is for admin role only. Admin role can view all information of a single user that has registered on the database.
*/
module.exports.getProfileNew = (userData) => {
	return User.findById(userData.userId).then(result => {
		return result;
	})
}

// Get Single User - all information
/*
@desc This is for admin role only. Admin role can view all information of a single user that has registered on the database.
*/
module.exports.getUserProfile = (data, userId) => {
	if(data.isAdmin){
		return User.findById(userId, {
			password: 0
		}).then(result => {
			return result
		})
	}else {

		let message = Promise.resolve(false)
		return message.then((value) => {
			return value
		})
	}
}

// Get Single User - filtered information
/*
@desc Admin role or non-admin role can view single user's filtered information that has registered on the database.
*/
module.exports.getProfile = (userId) => {
	return User.findById(userId, {
		phoneNumber: 0,
		password: 0,
		isAdmin: 0,
		isSeller: 0,
		shop: 0,
		orders: 0,
		timestamps: 0,
		__v: 0,
		updatedAt: 0,
		createdAt: 0
	}).then(result => {
		return result;
	})
}


// Set a user as admin
/*
@desc This is for admin role only. Admin role can set a non-admin role user to admin role.
*/
module.exports.setAsAdmin = (userData, userId) => {
	if(userData.isAdmin) {
		return User.findByIdAndUpdate(
			userId, {
				isAdmin: true
			})
			.then((updatedRole, error) => {
				if(error) {
					return false
				}else {
					return true
				}
			})
	}else {
		let message = Promise.resolve(false)
		return message.then((value) => {
			return value
		})
	}
}

// Set an admin as user
/*
@desc This is for admin role only. Admin role can set an admin role back to user role.
*/
module.exports.setAsUser = (userData, userId) => {
	if(userData.isAdmin) {
		return User.findByIdAndUpdate(
			userId, {
				isAdmin: false
			})
			.then((updatedRole, error) => {
				if(error) {
					return false
				}else {
					return true
				}
			})
	}else {
		let message = Promise.resolve(false)
		return message.then((value) => {
			return value
		})
	}
}


// Deleting a user
/*
@desc 
*/
module.exports.deleteUser = (token, userId) => {
	if(token.isAdmin) {
		return User.findByIdAndDelete(userId)
		.then((result) => {
			if(result) {
				return true
			}else {
				return false
			}
		})
	}else {

		let message = Promise.resolve(false)
		return message.then((value) => {
			return value
		})
	}
}

// Create an Order
/*
@desc 
*/
module.exports.createOrder = async (req, res) => {

	const userData = auth.decode(req.headers.authorization)


	if(userData.isAdmin){
		return res.send("You don't have the authorization to order!");
	}
	else{
		let isUserUpdated = await User.findById(userData.id).then(user => {
		user.orders.push({
		totalAmount: req.body.totalAmount,
		products: req.body.products
		})

		// Save the updated user information in the database
		return user.save()
		.then(result => {
			res.send(`Thank you for ordering!`);
		})
		.catch(error => {
			console.log(error);
			res.send(error);
			})
		})
		// console.log(isUserUpdated)
		for (let i=0; i < req.body.products.length; i++){

		let data = {
			userId: userData.id,
			productId: req.body.products[i].productId,
			productName: req.body.products[i].productName,
			quantity: req.body.products[i].quantity
		}

		let isProductUpdated = await Product.findById(data.productId).then(product => {
		product.orders.push({
			userId: data.userId,
			quantity: req.body.quantity,
		})

		// Minus the stocks available by quantity from req.body
		product.stocks -= data.quantity;

		return product.save()
		.then(result => {
			console.log(result);
			return true;
		})
		.catch(error => {
			console.log(error);
			return false;
				})
			// Condition will check if the both "user" and "product" document have been updated.
			//(isUserUpdated && isProductUpdated) ? res.send(true) : res.send(false)

			})
		}

	}
}


// Add to cart
/*
@desc 
*/
module.exports.addToCart = async (data) => {
	if(!data.isAdmin) {
		let isUserUpdated = await User.findById(data.userId).then(user => {
			user.orders.push({productId: data.productId, 
				totalAmount: data.totalAmount,
				quantity: data.quantity});

			return user.save().then((user, error) => {
				if(error) {
					return false;
				}else {
					return true
				}
			})
		})

		let isOrderUpdated = await Product.findById(data.productId).then(product => {
			product.orders.push({ userId: data.userId, quantity: data.quantity});

			return product.save().then((product, error) => {
				if(error){
					return false
				}else {
					return true
				}
			})
		})

		if(isUserUpdated && isOrderUpdated) {
			return `Successfully added to your cart.`;
		}else {
			return false
		}
	}else {
		return false;
	}
}
		

/*module.exports.addToCart = async (data) => {
	if(data.isAdmin) {
		return `You are not allowed to perform this task.`
	}else {
		let isUserUpdated = await User.findById(data.userId).then(user => {
			user.orders.push({productId: data.productId, quantity: data.quantity});

			return user.save().then((user, error) => {
				if(error) {
					return false;
				}else {
					return true
				}
			})
		})

		let isOrderUpdated = await Product.findById(data.productId).then(product => {
			product.orders.push({orderId: data.orderId, userId: data.userId});

			return product.save().then((product, error) => {
				if(error){
					return false
				}else {
					return true
				}
			})
		})

		if(isUserUpdated && isOrderUpdated) {
			return true;
		}else {
			return false
		}
	}
}*/

// User cart
/*
@desc 
*/
module.exports.getUserCart = (userId) => {
	return User.findById(userId, {
		phoneNumber: 0,
		password: 0,
		isAdmin: 0,
		isSeller: 0,
		shop: 0,
		timestamps: 0,
		__v: 0,
		updatedAt: 0,
		createdAt: 0
	}).then(result => {
		return result;
	})
}

/*
	TRIAL -----------------------------------------------------------------------
*/
// Add to cart
/*
@desc 
*/
module.exports.addToCartTrial = async (data) => {
	console.log(data);
	if(!data.isAdmin) {
	console.log(data.isAdmin);
		let isUserUpdated = await User.findById(data.userId).then(user => {
			console.log(data.userId);
			console.log(data.products);
			/*console.log(data.products[0]);
			console.log(data.product[0].productId);*/
			user.orders.push({productId: data.products[0].productId, 
				totalAmount: data.totalAmount,
				quantity: data.products[1].quantity});

			return user.save().then((user, error) => {
				if(error) {
					return false;
				}else {
					return true
				}
			})
		})

		let isOrderUpdated = await Product.findById(data.productId).then(product => {
			product.orders.push({ userId: data.userId, quantity: data.quantity});

			return product.save().then((product, error) => {
				if(error){
					return false
				}else {
					return true
				}
			})
		})

		if(isUserUpdated && isOrderUpdated) {
			return `Successfully added to your cart.`;
		}else {
			return false
		}
	}else {
		let message = Promise.resolve(`You are not allowed to perform this task.`)
		return message.then((value) => {
			return value
		})
	}
}

// User cart
/*
@desc 
*/
module.exports.getUserCartTrial = (userId) => {
	return User.findById(userId, {
		phoneNumber: 0,
		password: 0,
		isAdmin: 0,
		isSeller: 0,
		shop: 0,
		timestamps: 0,
		__v: 0,
		updatedAt: 0,
		createdAt: 0
	}).then(result => {
		return result;
	})
}