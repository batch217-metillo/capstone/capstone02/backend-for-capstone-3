/*
	Entry Point for Registration and Authentication
*/

const User = require("../models/userModel.js");
const bcrypt = require("bcrypt");
const auth = require("../auth.js");

// Checking Email if Exists
/*
@desc It is used to check if an email has already been taken.
*/
module.exports.checkEmailExists = (reqBody) => {
	return User.find({emailAddress: reqBody.emailAddress}).then(result => {
		if(result.length > 0) {
			return true;
		}else{
			return false
		}
	})
}

// Checking Username if Exists
/*
@desc It is used to check if an username has already been taken.
*/
module.exports.checkUsernameExists = (reqBody) => {
	return User.find({username: reqBody.username}).then(result => {
		if(result.length > 0) {
			return true;
		}else{
			return false
		}
	})
}

// New User Registration
/*
@desc User cannot register using the same email address.
*/
module.exports.registerUser = (reqBody) => {
	let newUser = new User({
		firstName : reqBody.firstName,
		lastName : reqBody.lastName,
		username: reqBody.username,
		emailAddress: reqBody.emailAddress,
		phoneNumber: reqBody.phoneNumber,
		password: bcrypt.hashSync(reqBody.password, 10)
	})

	return newUser.save().then((user, error) => {
		if(error) {
			return false;
		}else {
			return true;
		}
	})
}

// User Registration
/*
@desc User cannot register using the same email address.
*/
module.exports.userRegistration = (reqBody, res) => {
	return User.findOne({$or : [{emailAddress: reqBody.emailAddress}, {username: reqBody.username}]}).then(result => {
		if(result !=null && result.emailAddress == reqBody.emailAddress && result.username == reqBody.username) {
			return false
		}else if(result !=null && result.emailAddress == reqBody.emailAddress) {
			return false
		}else if(result !=null && result.username == reqBody.username) {
			return false
		}else {
			let newUser = new User({
				firstName: reqBody.firstName,
				lastName: reqBody.lastName,
				username: reqBody.username,
				emailAddress: reqBody.emailAddress,
				phoneNumber: reqBody.phoneNumber,
				password: bcrypt.hashSync(reqBody.password, 10)
			});

			return newUser.save().then((saveUser, saveErr) => {
				if(saveErr) {
					return false;
				}else {
					return true
				}
			})
		}
	})
}

// User Login
/*
@desc User cannot login using incorrect email address or password
*/
module.exports.userLogin = (reqBody, res) => {
	return User.findOne({$or : [{emailAddress: reqBody.emailAddress}, {username: reqBody.username}]}).then(result => {
		if(result == null){
			return false;
		}else{
			const isPasswordCorrect = bcrypt.compareSync(reqBody.password, result.password)

			if(isPasswordCorrect){
				// Generate an access
				return {access: auth.createAccessToken(result)}
			} else {
				return false
			}
		}
	})
}